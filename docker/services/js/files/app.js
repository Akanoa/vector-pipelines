const winston = require('winston');

const { combine, timestamp, label, printf } = winston.format;

const myFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = winston.createLogger({
  level: 'info',
  format: combine(
    label({ label: 'main' }),
    timestamp(),
    myFormat
  ),
  transports: [
    new winston.transports.Console(),
  ],
});

logger.info("Application starting...")
logger.info("Application started")
logger.debug("Using default configuration")
logger.warn("Missing argument")
logger.error("Emergency application stop!")