# Vector Log Pipelines

## Docker

This pipeline use a fluent server a log source. I've tried the syslog one but for some messages
the `appname` field wasn't correctly defined. Even using the recommended RFC5424 format.

Fluent is more robust and gives reliable tag value.

The docker stack as example purpose can be found inside the docker's folder.

To create the the missing image launch

```bash
docker pull tomcat
docker tag tomcat tomcat:1.0.0
docker tag tomcat tomcat_toto
```

`$HOSTNAME` is simply the `$(hostname)` but docker-compose doesn't allow running bash expression inside the configuration file.

The idea of this pipeline is to handle the most message formats and output a normalize message event containing:

- the full container name : Image/containerID
- the hostname
- severity
- extracted message

To ease the search of log files, there are stored following a pattern that involves the hostname and image name.

To try various type of logging message formats, I used both a tomcat and a jetty image.

There the type of message output by the first one

```
13-Sep-2021 05:47:49.513 INFO [main] org.apache.coyote.AbstractProtocol.init Initializing ProtocolHandler ["http-nio-8080"]
```

And there for the second one:

```
2021-09-13 05:47:47.584:INFO:oejs.AbstractConnector:main: Started ServerConnector@41dddee8{HTTP/1.1, (http/1.1)}{0.0.0.0:8080}
```

As you can see, all the data are present but not represented with the same format.

The challenge of my pipeline is to get all this kind of messages and normalize them to something more easily handleable by futher processes.

That the work of the two regexps:

For tomcat

```
^\d{2}-\w{3}-\d{4}\s(?:\d{2}:){2}\d{2}.\d{3}\s(?P<severity>\w+)\s\[(?P<section>[\w-]+)\]\s(?P<path>[^\s]+)\s(?P<message>.*)$
```

For Jetty

```
^\d{4}-\d{2}-\d{2}\s(?:\d{2}:){2}\d{2}.\d{3}:(?P<severity>\w+):(?P<path>[\w\.-]+):(?P<section>[\w-]+):\s(?P<message>.*)$
```

Vector allow to chain try parsing with a coalescence operator like this:

```
result = parse_regex(field, r'regex1') ??
         parse_regex(field, r'regex2') ??
         "invalid"
```

And even define a fallback value.

That's way you can create a normalization system of various sources.

Caution: although regex are pretty fast, chaining them will slow down your process.

It's maybe a clever way to create filters based on tags provide by the message event and treat messages following there topology instead of brute-forcing the 
format recognition through regex chaining.


As output the system will give you meassages like this.

```json
{"container_id":"702642b3b7faf0596ebb593ab12a89d06965292a322add9c50dbbf0e81a5bbc9","container_name":"/root_tomcat-1_1","facility":"docker","host":"192.168.9.61","hostname":"docker-001.lab.log","image":"tomcat:1.0.0","message":"13-Sep-2021 06:09:20.570 INFO [main] org.apache.catalina.startup.Catalina.load Server initialization in [1297] milliseconds","message_extracted":"Server initialization in [1297] milliseconds","path":"org.apache.catalina.startup.Catalina.load","section":"main","severity":"INFO","source":"stderr","tag":"docker-001.lab.log/tomcat:1.0.0/702642b3b7fa","timestamp":"2021-09-13T06:09:20Z","unix_timestamp":1631513360000}
```